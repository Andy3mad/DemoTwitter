package andyapps.com.demo.Activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import andyapps.com.demo.Adapters.FollowersAdapter;
import andyapps.com.demo.Adapters.FollowersGridAdapter;
import andyapps.com.demo.Constants;
import andyapps.com.demo.Helper;
import andyapps.com.demo.InterfaceAdapter;
import andyapps.com.demo.PrefManager;
import andyapps.com.demo.R;
import andyapps.com.demo.Users;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

public class UserFollowersActivity extends AppCompatActivity {
    PrefManager prefManager;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private FollowersAdapter followersAdapter;
    private FollowersGridAdapter followersGridAdapter;
    private LinearLayout llContainer;
    private ProgressBar progressBar;
    private TextView tvError;
    private GridView gridView;
    boolean isPortriat = true;
    Helper helper;
    private String user_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_followers);
        prefManager = new PrefManager(this);
        helper = new Helper();
        isPortriat = helper.isScreenPortrait(this);
        if (isPortriat) {
            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        } else {
            gridView = (GridView) findViewById(R.id.grid);
        }
        user_id = prefManager.getData(Constants.USER_ID);
        llContainer = (LinearLayout) findViewById(R.id.ll_container);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swip_to_refresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getFollowerData();
            }
        });
        tvError = (TextView) findViewById(R.id.tv_error);
        llContainer.setVisibility(View.GONE);
        tvError.setVisibility(View.GONE);
        getFollowerData();
    }

    private void getFollowerData() {
        if (helper.isNetworkOnline(this))
            new getFollowersList().execute();
        else {
            Type listType = new TypeToken<List<Users>>() {
            }.getType();
            List<Users> users = new Gson().fromJson(prefManager.getData(Constants.FOLLOWERS), listType);
            if (users != null) {
                if (isPortriat)
                    handleFollowersToRecycleView(users);
                else
                    handleFollowersToGrid(users);
            }
        }
    }

    private void handleFollowersToGrid(List<Users> users) {
        followersGridAdapter = new FollowersGridAdapter(UserFollowersActivity.this, users);
        gridView.setAdapter(followersGridAdapter);
        llContainer.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        gridView.setVisibility(View.VISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }

    private void handleFollowersToRecycleView(List<Users> users) {
        followersAdapter = new FollowersAdapter(users, UserFollowersActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(followersAdapter);
        llContainer.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.GONE);
        swipeRefreshLayout.setRefreshing(false);

    }


    class getFollowersList extends AsyncTask<Void, Boolean, List<Users>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected List<Users> doInBackground(Void... args) {

            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(getResources().getString(R.string.twitter_consumer_key));
                builder.setOAuthConsumerSecret(getResources().getString(R.string.twitter_consumer_secret));

                // Access Token
                String access_token = prefManager.getData(Constants.PREF_KEY_OAUTH_TOKEN);
                // Access Token Secret
                String access_token_secret = prefManager.getData(Constants.PREF_KEY_OAUTH_SECRET);

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                try {
                    List<twitter4j.User> users = twitter.getFollowersList(twitter.getId(), -1);
                    List<Users> usersList = new ArrayList<>();
                    usersList = helper.convertUserToUsers(users);
                    return usersList;
                } catch (TwitterException e) {
                    e.printStackTrace();
                    return null;
                }

            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<Users> userList) {
            if (userList != null) {
                prefManager.put(Constants.FOLLOWERS, new Gson().toJson(userList));
                if (isPortriat)
                    handleFollowersToRecycleView(userList);
                else
                    handleFollowersToGrid(userList);
            } else {
                progressBar.setVisibility(View.GONE);
                tvError.setVisibility(View.VISIBLE);
            }

            // Clearing EditText field
        }
    }
}
