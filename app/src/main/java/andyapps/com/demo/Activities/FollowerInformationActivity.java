package andyapps.com.demo.Activities;

import android.app.Dialog;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import andyapps.com.demo.Adapters.TweetsAdapter;
import andyapps.com.demo.Constants;
import andyapps.com.demo.Helper;
import andyapps.com.demo.PrefManager;
import andyapps.com.demo.R;
import de.hdodenhof.circleimageview.CircleImageView;
import twitter4j.Paging;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;

public class FollowerInformationActivity extends AppCompatActivity {
    Long id;
    PrefManager prefManager;
    ImageLoader imageLoader;
    Helper helper;
    ImageView ivBackground;
    CircleImageView ivProfile;
    ProgressBar progressBar;
    RecyclerView recyclerView;
    TextView tvError;
    private TweetsAdapter mAdapter;
    String background, profile;
    private List<Status> statusList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follower_information);
        ivBackground = (ImageView) findViewById(R.id.iv_background);
        ivProfile = (CircleImageView) findViewById(R.id.iv_profile);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        tvError = (TextView) findViewById(R.id.tv_error);


        id = getIntent().getLongExtra(Constants.ID, -1);
        background = getIntent().getStringExtra(Constants.BACKGROUND);
        profile = getIntent().getStringExtra(Constants.PROFILE);
        recyclerView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        tvError.setVisibility(View.GONE);

        ivBackground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (background != null && background.length() > 0)
                    openPopup(background);
            }
        });

        ivProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profile != null && profile.length() > 0)
                    openPopup(profile);
            }
        });

        helper = new Helper();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);
        imageLoader = ImageLoader.getInstance();
        prefManager = new PrefManager(this);
        bindImages();
        if (id != -1 ) {
            new getFollowersList().execute();

        }
    }

    private void openPopup(String url) {
        Dialog dialog = new Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.image_view);
        ImageView imageView = (ImageView) dialog.findViewById(R.id.myZoomageView);
        helper.setImage(url, imageView, imageLoader, R.drawable.no_image);
        dialog.show();
    }

    private void bindImages() {
        helper.setImage(background, ivBackground, imageLoader, R.drawable.no_image);
        helper.setImage(profile, ivProfile, imageLoader, R.drawable.no_image);
    }


    class getFollowersList extends AsyncTask<Void, Boolean, List<Status>> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBar.setVisibility(View.VISIBLE);
        }

        protected List<twitter4j.Status> doInBackground(Void... args) {

            try {
                ConfigurationBuilder builder = new ConfigurationBuilder();
                builder.setOAuthConsumerKey(getResources().getString(R.string.twitter_consumer_key));
                builder.setOAuthConsumerSecret(getResources().getString(R.string.twitter_consumer_secret));

                // Access Token
                String access_token = prefManager.getData(Constants.PREF_KEY_OAUTH_TOKEN);
                String access_token_secret = prefManager.getData(Constants.PREF_KEY_OAUTH_SECRET);

                AccessToken accessToken = new AccessToken(access_token, access_token_secret);
                Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

                try {
                    List<twitter4j.Status> statusList = twitter.getUserTimeline(id , new Paging().count(10));
                    return statusList;
                } catch (TwitterException e) {
                    e.printStackTrace();
                    return null;
                }

            } catch (Exception e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(List<twitter4j.Status> statusList) {
            if (statusList != null) {
                progressBar.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                mAdapter = new TweetsAdapter(statusList);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
                recyclerView.setLayoutManager(mLayoutManager);
                recyclerView.setItemAnimator(new DefaultItemAnimator());
                recyclerView.setAdapter(mAdapter);
                progressBar.setVisibility(View.GONE);
            } else {
                progressBar.setVisibility(View.GONE);
                tvError.setVisibility(View.VISIBLE);
            }

        }
    }
}
