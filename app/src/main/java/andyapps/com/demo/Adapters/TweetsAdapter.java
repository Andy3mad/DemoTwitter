package andyapps.com.demo.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import java.util.List;

import andyapps.com.demo.R;
import twitter4j.Status;

/**
 * Created by DELL on 01/02/2018.
 */

public class TweetsAdapter extends RecyclerView.Adapter<TweetsAdapter.MyViewHolder> {

    private List<Status> tweetList;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView  tvTweet;

        public MyViewHolder(View view) {
            super(view);
            tvTweet = (TextView) view.findViewById(R.id.tv_tweet);
        }
    }


    public TweetsAdapter(List<Status> tweetList ) {
        this.tweetList = tweetList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tweet_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvTweet.setText(tweetList.get(position).getText());
    }

    @Override
    public int getItemCount() {
        return tweetList.size();
    }
}