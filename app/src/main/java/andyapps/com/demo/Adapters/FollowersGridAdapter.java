package andyapps.com.demo.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import andyapps.com.demo.Activities.FollowerInformationActivity;
import andyapps.com.demo.Constants;
import andyapps.com.demo.Helper;
import andyapps.com.demo.R;
import andyapps.com.demo.Users;

/**
 * Created by DELL on 01/02/2018.
 */
public class FollowersGridAdapter extends BaseAdapter {
    private final List<Users> userList;
    Context context;
    LayoutInflater inflter;
    Helper helper ;
    ImageLoader imageLoader ;
    public FollowersGridAdapter(Context applicationContext, List<Users> userList) {
        this.context = applicationContext;
        this.userList = userList;
        inflter = (LayoutInflater.from(applicationContext));
        helper = new Helper();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        ImageLoader.getInstance().init(config);
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return userList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.followers_grid_row, null); // inflate the layout
        ImageView ivProfile = (ImageView) view.findViewById(R.id.iv_profile); // get the reference of ImageView
        TextView tvName = (TextView) view.findViewById(R.id.tv_name);
        TextView tvBio = (TextView) view.findViewById(R.id.tv_bio);
        tvName.setText(userList.get(i).name);
        tvBio.setText(userList.get(i).bio);
        helper.setImage(userList.get(i).profile_pic , ivProfile , imageLoader , R.drawable.placeholder);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context , FollowerInformationActivity.class);
                intent.putExtra(Constants.ID, userList.get(i).id);
                intent.putExtra(Constants.PROFILE, userList.get(i).profile_pic);
                intent.putExtra(Constants.BACKGROUND, userList.get(i).background_pic);
                context.startActivity(intent);
            }
        });
        return view;
    }
}