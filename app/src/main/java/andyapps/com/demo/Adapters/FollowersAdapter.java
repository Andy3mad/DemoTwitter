package andyapps.com.demo.Adapters;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.util.List;

import andyapps.com.demo.Constants;
import andyapps.com.demo.Activities.FollowerInformationActivity;
import andyapps.com.demo.Helper;
import andyapps.com.demo.R;
import andyapps.com.demo.Users;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by DELL on 01/02/2018.
 */

public class FollowersAdapter extends RecyclerView.Adapter<FollowersAdapter.MyViewHolder> {

    private List<Users> followersList;
    Helper helper ;
    ImageLoader imageLoader ;
    Activity activity ;


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView  tvName, tvBio;
        public CircleImageView ivProfile ;

        public MyViewHolder(View view) {
            super(view);
            ivProfile = (CircleImageView) view.findViewById(R.id.iv_profile);
            tvName = (TextView) view.findViewById(R.id.tv_name);
            tvBio = (TextView) view.findViewById(R.id.tv_bio);
        }
    }


    public FollowersAdapter(List<Users> followersList , Activity activity) {
        this.followersList = followersList;
        this.activity = activity ;
        helper = new Helper();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(activity).build();
        ImageLoader.getInstance().init(config);
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.follower_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Users user = followersList.get(position);
        holder.tvName.setText(user.name);
        holder.tvBio.setText(user.bio);
        helper.setImage(user.profile_pic , holder.ivProfile , imageLoader , R.drawable.twitter_placeholder);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity , FollowerInformationActivity.class);
                intent.putExtra(Constants.ID, user.id);
                intent.putExtra(Constants.PROFILE, user.profile_pic);
                intent.putExtra(Constants.BACKGROUND, user.background_pic);
                activity.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return followersList.size();
    }
}