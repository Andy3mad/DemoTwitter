package andyapps.com.demo;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import twitter4j.User;

/**
 * Created by DELL on 09/06/2017.
 */

public class Helper {

    public static void setImage(String url, ImageView view, ImageLoader imageLoader, int no_image) {


        if (URLUtil.isValidUrl(url)) {
            File file = imageLoader.getDiskCache().get(url);
            if (!file.exists()) {
                DisplayImageOptions options = new DisplayImageOptions.Builder().showImageForEmptyUri(no_image)
                        .cacheInMemory(true).cacheOnDisk(true)
                        .build();
                imageLoader.displayImage(url, view, options, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                        view.setBackgroundResource(R.drawable.loading);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        view.setBackgroundResource(R.drawable.no_image);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        ((ImageView) view).setImageBitmap(loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {

                    }
                });

            } else {
                view.setImageURI(Uri.parse(file.getAbsolutePath()));
            }

        }
    }

    public boolean isNetworkOnline(Activity activity) {
        boolean status=false;
        try{
            ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getNetworkInfo(0);
            if (netInfo != null && netInfo.getState()==NetworkInfo.State.CONNECTED) {
                status= true;
            }else {
                netInfo = cm.getNetworkInfo(1);
                if(netInfo!=null && netInfo.getState()==NetworkInfo.State.CONNECTED)
                    status= true;
            }
        }catch(Exception e){
            e.printStackTrace();
            return false;
        }
        return status;

    }

    public boolean isScreenPortrait(Activity activity)
    {
        int display_mode = activity.getResources().getConfiguration().orientation;

        return  (display_mode == Configuration.ORIENTATION_PORTRAIT) ;
    }


    public List<Users> convertUserToUsers(List<User> users){
        List<Users> usersList = new ArrayList<>();
        for (User user: users
             ) {
            Users users1 = new Users();
            users1.name = user.getName();
            users1.id = user.getId();
            users1.background_pic = user.getProfileBackgroundImageUrlHttps();
            users1.bio = user.getDescription();
            users1.profile_pic  = user.getProfileImageURLHttps();
            usersList.add(users1);
        }
        return usersList ;
    }

}
