package andyapps.com.demo;

import android.app.Activity;
import android.content.SharedPreferences;


/**
 * Created by eslam on 12/30/16.
 */

public class PrefManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Activity _context;

    // shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = Constants.PREF_NAME;

    public PrefManager(Activity context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void put(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getData(String key) {
        return pref.getString(key, "");
    }


}
